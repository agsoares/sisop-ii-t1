#include <stdlib.h>
#include <stdio.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

typedef struct matrix_t {
  int lin;
  int col;
  int *val;
} matrix_t;

int num_threads = 1;
int segment_id;
pid_t *pids;

matrix_t *in1, *in2, *out;

void alloc(matrix_t* matrix) {
  matrix->val = malloc(matrix->col*matrix->lin*sizeof(int));
}

void allocShared(matrix_t* matrix) {
  /* allocate shared memory segment */   
  segment_id = shmget(IPC_PRIVATE, matrix->col*matrix->lin*sizeof(int), S_IRUSR | S_IWUSR);  
}

void read(matrix_t* matrix, char* path) {
  FILE *file;
  file = fopen(path, "r");
  if (file == NULL) {
    printf("ERRO: \n");
    exit(-1);
  }
  fscanf(file, "LINHAS = %d\n", &matrix->lin);
  fscanf(file, "COLUNAS = %d\n", &matrix->col);
  
  alloc(matrix);
  
  int lin, col;
  for (lin = 0; lin < matrix->lin; lin++) {
    for (col = 0; col < matrix->col; col++) {
      if (!fscanf(file, "%d", &matrix->val[lin*matrix->col+col])) 
        break;
    }
    
  }
  fclose(file);
  
}

void write(char* path) {
  FILE *file;
  file = fopen(path, "w");
  if (file == NULL) {
    printf("ERRO: \n");
    exit(-1);
  }
  fprintf(file, "LINHAS = %d\n", out->lin);
  fprintf(file, "COLUNAS = %d\n", out->col);
  out->val = (int *) shmat(segment_id, NULL, 0);
  int lin, col;
  for (lin = 0; lin < out->lin; lin++) {
    for (col = 0; col < out->col; col++) {
      fprintf(file, "%d ", out->val[lin*out->col+col]);
    }
    fprintf(file,"\n");
  }
  fclose(file);
}

void *multiply(void* arg) {
  int index = *((int *)arg);
  out->val = (int *) shmat(segment_id, NULL, 0);
  int lin, col, i;
  for (lin = 0; lin < out->lin; lin++) {
    if (lin%num_threads == index) {
      for (col = 0; col < out->col; col++) {
        out->val[lin*out->col+col] = 0;
        for (i = 0; i < in1->col; i++) {
          out->val[lin*out->col+col] += in1->val[lin*in1->col+i] * in2->val[i*in2->col+col];
        }
      }
    }
  }
}

int main(int argc,char *argv[]) {
  if (argc < 2) {
    printf("ERRO: \n");
    return -1;
  }
  
  num_threads = atoi(argv[1]);
  pids = malloc(sizeof(pid_t)*num_threads);
  
  in1 = malloc(sizeof(matrix_t));
  in2 = malloc(sizeof(matrix_t));
  out = malloc(sizeof(matrix_t));
  
  if (argc >= 4) {
    read(in1, argv[2]);
    read(in2, argv[3]); 
  } else {
    read(in1, "in1.txt");
    read(in2, "in2.txt");
  }

  if (in1->col != in2->lin) {
    printf("ERRO: Matrizes de tamanho incopativeis\n");
    return -1;    
  }
  
  out->lin = in1->lin;
  out->col = in2->col;
  allocShared(out);
  
  int i;
  for (i = 0; i < num_threads; i++) {
    int *arg = malloc(sizeof(int));
    *arg = i;
    pids[i] = fork();
    if (pids[i] == 0) {
      multiply ((void *)(arg));
      exit(0);
    }
  }
  for (i = 0; i < num_threads; i++) {
    waitpid(pids[i], 0, 0);
  }
  
  if (argc >= 5) {
    write(argv[4]);
  } else {
    write("out.txt");
  }
  
  return 0;
}