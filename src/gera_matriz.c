#include <stdlib.h>
#include <stdio.h>
#include <time.h>

typedef struct matrix_t {
  int lin;
  int col;
  int *val;
} matrix_t;

void alloc(matrix_t* matrix) {
  matrix->val = malloc(matrix->col*matrix->lin*sizeof(int));
}
matrix_t* out;

void write(char* path) {
  FILE *file;
  file = fopen(path, "w");
  if (file == NULL) {
    printf("ERRO: \n");
    exit(-1);
  }
  fprintf(file, "LINHAS = %d\n", out->lin);
  fprintf(file, "COLUNAS = %d\n", out->col);
  int lin, col;
  for (lin = 0; lin < out->lin; lin++) {
    for (col = 0; col < out->col; col++) {
      fprintf(file, "%d ", out->val[lin*out->col+col]);
    }
    fprintf(file,"\n");
  }
  fclose(file);
}

int main(int argc,char *argv[]) {
  if (argc < 4) {
   return -1; 
  }
  out = malloc(sizeof(matrix_t));
  out->lin = atoi(argv[1]);
  out->col = atoi(argv[2]);
  
  alloc(out);
  srand((unsigned)time(NULL));
  int lin, col;
  for (lin = 0; lin < out->lin; lin++) {
   for (col = 0; col < out->col; col++) {
     out->val[lin*out->col+col] = rand()%100;
   }
    
  }
  
  
  write(argv[3]);
  
  
  
  
  return 0;
}